<?php

namespace Drupal\webform_email_confirmation_link\Controller;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform_email_confirmation_link\Plugin\WebformHandler\EmailConfirmationWebformHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Controller for webform email confirmation.
 */
class WebformEmailConfirmationController extends ControllerBase {

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The webform token manager.
   *
   * @var \Drupal\webform\WebformTokenManagerInterface
   */
  protected $webformTokenManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->messenger = $container->get('messenger');
    $instance->entityRepository = $container->get('entity.repository');
    $instance->time = $container->get('datetime.time');
    $instance->webformTokenManager = $container->get('webform.token_manager');

    return $instance;
  }

  /**
   * The webform confirmation route.
   *
   * @param string $uuid
   *   The webform submission UUID.
   * @param int $timestamp
   *   The webform submission timestamp.
   * @param string $hash
   *   The hash of the UUID and timestamp.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirect to homepage.
   */
  public function confirmation(string $uuid, int $timestamp, string $hash) {
    $redirectUrl = Url::fromRoute('<front>')->toString();

    $submission = $this->entityRepository->loadEntityByUuid('webform_submission', $uuid);
    if (!$submission instanceof WebformSubmissionInterface) {
      return new RedirectResponse($redirectUrl);
    }

    $handler = webform_email_confirmation_link_get_handler($submission);
    if (!$handler instanceof EmailConfirmationWebformHandler) {
      return new RedirectResponse($redirectUrl);
    }

    if ($redirectPath = $handler->getSetting('redirect_path')) {
      $redirectPath = $this->webformTokenManager->replace($redirectPath, $submission);
      if (!UrlHelper::isValid($redirectPath, TRUE)) {
        try {
          $redirectPath = Url::fromUserInput($redirectPath)->toString();
        }
        catch (\InvalidArgumentException $e) {
          $redirectPath = '';
        }
      }
      if (UrlHelper::isValid($redirectPath, TRUE)) {
        $redirectUrl = $redirectPath;
      }
    }

    $timeout = (int) $handler->getSetting('confirmation_url_timeout');
    if (!$this->validatePathParameters($submission, $timestamp, $hash, $timeout)) {
      $this->messenger->addError($this->t('You have tried to use a confirmation link that has either been used, is invalid or has expired.'));
      return new RedirectResponse($redirectUrl);
    }

    $requestTime = $this->time->getRequestTime();
    $submission->setCompletedTime($requestTime);
    $submission->setChangedTime($requestTime);
    $submission->set('in_draft', FALSE);
    $submission->save();

    $this->messenger->addMessage($handler->getSetting('confirmation_message'));
    return new RedirectResponse($redirectUrl);
  }

  /**
   * Validates hash and timestamp.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $submission
   *   The webform submission.
   * @param int $timestamp
   *   The timestamp.
   * @param string $hash
   *   The hash of the UUID and timestamp.
   * @param int $timeout
   *   Link expiration timeout.
   *
   * @return bool
   *   Whether the provided data are valid.
   */
  protected function validatePathParameters(WebformSubmissionInterface $submission, int $timestamp, string $hash, int $timeout = 0): bool {
    if (!$submission->isDraft()) {
      return FALSE;
    }

    $currentTime = $this->time->getRequestTime();

    // Determine if the timeout is valid (either no timeout or within the
    // allowed time range).
    $isTimeoutValid = (empty($timeout) || ($currentTime - $timestamp < $timeout));
    // Determine if the timestamp is valid (within the submission creation
    // time and the current time).
    $isTimestampValid = ($timestamp >= $submission->getCreatedTime() && $timestamp <= $currentTime);
    // Determine if the hash is valid.
    $isHashValid = hash_equals($hash, webform_email_confirmation_link_rehash($submission, $timestamp));

    return $isTimestampValid && $isTimeoutValid && $isHashValid;
  }

}
