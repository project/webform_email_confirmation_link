<?php

namespace Drupal\webform_email_confirmation_link\Plugin\WebformHandler;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandler\EmailWebformHandler;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Emails a webform submission confirmation.
 *
 * @WebformHandler(
 *   id = "email_confirmation",
 *   label = @Translation("Email confirmation"),
 *   category = @Translation("Notification"),
 *   description = @Translation("Sends a webform submission confirmation via an email."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 *   tokens = TRUE,
 * )
 */
class EmailConfirmationWebformHandler extends EmailWebformHandler {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'states' => [WebformSubmissionInterface::STATE_DRAFT_CREATED],
      'subject' => static::OTHER_OPTION,
      'body' => static::OTHER_OPTION,
      'confirmation_url_timeout' => NULL,
      'redirect_path' => '/',
      'confirmation_message' => 'Your submission has been confirmed.',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $summary = parent::getSummary();

    $summary['#settings']['confirmation_url_timeout'] = $this->configuration['confirmation_url_timeout']
      ? $this->formatDuration($this->configuration['confirmation_url_timeout'])
      : $this->t('Never');

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  protected function getBodyDefaultValues($format = NULL) {
    $formats = [
      'text' => '[webform_submission:confirmation_link]',
      'html' => '[webform_submission:confirmation_link]',
    ];
    return ($format === NULL) ? $formats : $formats[$format];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['additional']['states']['#default_value'] = [WebformSubmissionInterface::STATE_DRAFT_CREATED];
    $form['additional']['states']['#access'] = FALSE;

    $form['additional']['confirmation_url_timeout'] = [
      '#type' => 'number',
      '#title' => $this->t('Confirmation URL expiration'),
      '#description' => $this->t('The number of seconds until the confirmation URL expires. Leave empty for no expiration.'),
      '#default_value' => $this->configuration['confirmation_url_timeout'],
      '#required' => TRUE,
      '#min' => 0,
    ];

    $form['additional']['redirect_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Redirect path'),
      '#description' => $this->t('The path to redirect to after the confirmation URL is visited.'),
      '#default_value' => $this->configuration['redirect_path'],
      '#required' => TRUE,
    ];

    $form['additional']['confirmation_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Confirmation message'),
      '#description' => $this->t('The message to display when the confirmation URL is visited.'),
      '#default_value' => $this->configuration['confirmation_message'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    $redirectPath = $form_state->getValue('additional')['redirect_path'];
    if (!UrlHelper::isValid($redirectPath)) {
      $form_state->setError($form['additional']['redirect_path'], $this->t('The redirect path is not a valid URL.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['confirmation_url_timeout'] = $form_state->getValue('additional')['confirmation_url_timeout'];
    $this->configuration['redirect_path'] = $form_state->getValue('additional')['redirect_path'];
    $this->configuration['confirmation_message'] = $form_state->getValue('additional')['confirmation_message'];
  }

  /**
   * Formats the duration in seconds into a human-readable string.
   *
   * @param int $seconds
   *   The duration in seconds.
   *
   * @return string
   *   The formatted duration.
   */
  protected function formatDuration(int $seconds): string {
    $years = abs(floor($seconds / 31536000));
    $days = abs(floor(($seconds - ($years * 31536000)) / 86400));
    $hours = abs(floor(($seconds - ($years * 31536000) - ($days * 86400)) / 3600));
    $minutes = abs(floor(($seconds - ($years * 31536000) - ($days * 86400) - ($hours * 3600)) / 60));

    $parts = [];
    if ($years > 0) {
      $parts[] = $years . ' years';
    }
    if ($days > 0) {
      $parts[] = $days . ' days';
    }
    if ($hours > 0) {
      $parts[] = $hours . ' hours';
    }
    if ($minutes > 0) {
      $parts[] = $minutes . ' minutes';
    }

    return implode(', ', $parts);
  }

}
